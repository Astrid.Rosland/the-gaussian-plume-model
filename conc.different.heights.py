import numpy as np
import matplotlib.pyplot as plt
from function import stabilityclass
from function import sigmas
from function import C_get
from function import u_star
from function import U_funk

## get u_star
u_ref=10
z_ref=10
z0=0.03
fric_vel=u_star(u_ref,z_ref,z0)
print(fric_vel)

#get u_mean at three different heights
z_so=np.array([25,50,100])

U_mean=U_funk(fric_vel,z_so,z0)

for i in range(0,U_mean.size):
    print((U_mean[i]))

#plot consentration at different heights
S0= 5
z= 0
y= 0
x_max=3000
stability= 'D'
x=np.linspace(0,x_max,1000)
C=np.zeros([z_so.size,1000])

fig, ax= plt.subplots(figsize=(10,10))
for i in range(0,z_so.size):
    C[i,:]= C_get(S0,U_mean[i],z_so[i],x,y,z,stability);
    plt.plot(x,C[i,:])

plt.ticklabel_format(axis='y',style="sci", scilimits=(0,0),useMathText=True)
plt.ylim(0,)
ax.legend(np.array(['h=25 m', 'h=50 m', 'h=100 m']),fontsize=15)

plt.tick_params(labelsize=15)
plt.xlabel('x (m)',fontsize=15)
plt.ylabel('C(x) (g s$^{-1}$)',fontsize=15)
ax.yaxis.get_offset_text().set_fontsize(15)

#relative error
x = 1500
y = 0
z = 0
stability = 'D'

C_correct = np.zeros([z_so.size,1])
C_wrong = np.zeros([z_so.size,1])

for i in range(0,z_so.size):
    C_correct[i,:] = C_get(S0,U_mean[i],z_so[i],x,y,z,stability);
    C_wrong[i,:] = C_get(S0,u_ref,z_so[i],x,y,z,stability);


relative_error = 100*((C_wrong-C_correct)/C_correct)

for i in range(0,relative_error.size):
    print(relative_error[i])

