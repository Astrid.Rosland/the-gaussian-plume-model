import numpy as np

######################### function for stabilityclass ##########################
def stabilityclass(A,B):
    """
    Compare two string arguments to check for equality.

    Args:
        A (str): The first input string for comparison.
        B (str): The second input string for comparison.

    Returns:
        str: 'true' if the lowercase versions of A and B are equal, 'false' otherwise.

    This function compares the two input strings, A and B. It first converts both
    strings to lowercase to ensure consistent comparison. If the lowercase versions are equal, the function
    assigns the string 'true' to the variable k; otherwise, it assigns 'false'. The result, which is either 'true'
    or 'false' is returned.
    """
    if A.lower() == B.lower():
        k = 'true'
    else:
        k = 'false'
    return k

############################ function for sigmas ###############################
def sigmas(stability,x):
    """
    Calculate values for sigma_y and sigma_z for different atmospheric stability classes.

    Args:
        stability (str): A string representing the atmospheric stability class ('A' to 'F').
        x (float): Downwind distance from the source.

    Returns:
        Tuple (float, float): Tuple containing sigma_y and sigma_z.

    This function calculates the values of sigma_y and sigma_z based on the provided atmospheric stability class('A' to 'F)
    and the downwind distance (x). The calculations involve the constants for each stability class and depend on the specific 
    stability conditions. If the provided stability class is not recognized ('A' to 'F'), the function raises a ValueError.
    """
    konst = (1 + 0.0001*x)**(-0.50);
    if stabilityclass(stability,'A')=='true':
            sig_y = 0.22*x*konst
            sig_z = 0.2*x
    elif stabilityclass(stability,'B')=='true':
            sig_y = 0.16*x*konst
            sig_z = 0.12*x
    elif stabilityclass(stability,'C')=='true':
            sig_y = 0.11*x*konst
            sig_z = 0.08*x*(1 + 0.0002*x)**(-0.50)
    elif stabilityclass(stability,'D')=='true':
            sig_y = 0.08*x*konst
            sig_z = 0.06*x*(1 + 0.0015*x)**(-0.50)
    elif stabilityclass(stability,'E')=='true':
            sig_y = 0.06*x*konst
            sig_z = 0.03*x*(1 + 0.0003*x)**(-1)
    elif stabilityclass(stability,'F')=='true':
            sig_y = 0.04*x*konst
            sig_z = 0.016*x*(1 + 0.0003*x)**(-1)
    else:
        raise ValueError('Unknown')
        
    return sig_y,sig_z

###########################function for get C ###########################################
def C_get(S0,u,h,x,y,z,stability):
    """
    Calculate pollutant concentration at a specified coordinate (x, y, z) based on the Gaussian plume equation.

    Args:
        S0 (float): the emission rate of the air pollutant at the source.
        u (float): mean wind speed at hight h
        h (float): Height of the source 
        x,y,z (float): coordinates where the pollutant concentration is calculated
        stability (str): stability class following Passquill-Gifford

    Returns:
       C (float): The pollutant concentration at the specified coordinate (x, y, z).
    """
    sig_y,sig_z= sigmas(stability,x)

    A = S0/(2*np.pi*u*sig_y*sig_z);
    B = np.exp(-y**2./(2*sig_y**2));
    D = np.exp(-(z-h)**2./(2*sig_z**2)) + np.exp(-(z+h)**2./(2*sig_z**2));
    C=A*B*D
    return C

##################### function for u_star ###################
def u_star(u_ref,z_ref,z0):
    kappa=0.4
    """
    Calculate the friction velocity (u_star) in a logarithmic wind profile.

    Args:
        u_ref (float): Reference wind speed at height z_ref.
        z_ref (float): Reference height above ground for u_ref.
        z0 (float): Surface roughness length.

    Returns:
        float: The friction velocity (u_star) for the given inputs.

    This function computes the friction velocity (u_star) based on the logarithmic wind profile equation,
    which relates wind speed at height z_ref to the surface wind speed and surface roughness length.
    The logarithmic wind profile is commonly used in atmospheric boundary layer studies to estimate
    wind speed at different heights above the ground. The calculated u_star is a critical parameter in
    characterizing turbulent transport in the atmosphere.
    """
    u_star=u_ref*kappa/(np.log(z_ref/z0))
    return u_star

######################### function for U ############################
def U_funk(u_star,z,z0):
     """
    Calculate the wind speed (U) at a given height in the atmospheric boundary layer.

    Args:
        u_star (float): Friction velocity.
        z (float): Height above ground for wind speed calculation.
        z0 (float): Surface roughness length.

    Returns:
        float: The wind speed (U) at the specified height.

    This function computes the wind speed (U) at a given height within the atmospheric boundary layer
    using the logarithmic wind profile equation. It takes into account the friction velocity (u_star),
    the height (z) at which you want to determine the wind speed, and the surface roughness length (z0).
    The logarithmic wind profile model is commonly employed in atmospheric science to estimate wind speeds
    at different altitudes above the Earth's surface, providing insights into atmospheric boundary layer dynamics.
    """
     kappa=0.4
     U=u_star/kappa*np.log(z/z0)
     return U

