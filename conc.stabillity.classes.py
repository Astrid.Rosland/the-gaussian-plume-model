import numpy as np
import matplotlib.pyplot as plt
from function import stabilityclass
from function import sigmas
from function import C_get

S0= 5
h= 30
u= 2
z= 0
y= 0
x_max=2500
stability= np.array(['A','B','C','D','E','F'])
x=np.linspace(0,x_max,1000)
s=stability.size
C=np.zeros([s,1000])



fig, ax= plt.subplots(figsize=(10,10))
for i in range(0,s):
    C[i,:]= C_get(S0,u,h,x,y,z,stability[i]);
    plt.plot(x,C[i,:])

plt.ticklabel_format(axis='y',style="sci", scilimits=(0,0),useMathText=True)
plt.ylim(0,)
ax.legend(np.array(['(A) Very unstable', '(B) Moderately unstable', '(C) slightly unstable',
       '(D) neutral', '(E) slightly stable', '(F) stable']),fontsize=15)

plt.tick_params(labelsize=15)
plt.xlabel('x (m)',fontsize=15)
plt.ylabel('C(x) (g s$^{-1}$)',fontsize=15)
ax.yaxis.get_offset_text().set_fontsize(15)
